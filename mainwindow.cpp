#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
  ui->setupUi(this);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_pbTransmitMessage_clicked()
{
  _transmitterData.clear();

  QTime start = QTime::currentTime();

  if (ui->ptOriginMessageText->toPlainText().size() == 0) {
      return;
  }

  QVector<real_T> receiverData;
  QVector<time_T> sendedSignal;
  QVector<time_T> time;

  int messageSize = ui->leMessageSize->text().toInt();

  QVector<QByteArray> recievedByteMessage;

  SignalHandler SH;
  QVector<QByteArray> byteMessage = SH.StrToBit(ui->ptOriginMessageText->toPlainText());

  SH.SendSignal(byteMessage, messageSize, _transmitterData, receiverData, sendedSignal, time);

  QVector<QByteArray> rawReceiverDataText = SH.ReceiveSignal(messageSize, receiverData, recievedByteMessage);
  QString receivedMessage = SH.BitToStr(rawReceiverDataText);

  QString stringByteReceivedText;
  for (int i = 0; i < recievedByteMessage.size(); ++i){
    stringByteReceivedText.append(recievedByteMessage[i] + "\n");
  }

  QString stringByteSendedText;
  for (int i = 0; i < byteMessage.size(); ++i) {
     stringByteSendedText.append(byteMessage[i] + '\n');
  }

  if (disableDetiledInfo) {

  } else {
    for (int i = 0; i < receiverData.size(); i += 100){
      _stringTransmitterDataText.append(QString::number(_transmitterData[i]) + "\n");
    }
    ui->ptCommunicationSignal->setPlainText(_stringTransmitterDataText);
  }

  ui->ptOriginMessageByteText->setPlainText(stringByteSendedText);
  ui->ptResieverMessageText->setPlainText(receivedMessage);
  ui->ptResieverMessageByteText->setPlainText(stringByteReceivedText);

  QString timeOfWork = msecToSec(start.elapsed());

  qDebug() << "Отправка сигнала:" << timeOfWork << "милисекунд.";
}

void MainWindow::on_pbShowChart_clicked()
{
  ChartWindow CW(_transmitterData);
  CW.setWindowFlags(CW.windowFlags() | Qt::WindowMinMaxButtonsHint);
  CW.exec();
}

QString MainWindow::msecToSec(int msec)
{
  QString strMsec = QString::number(msec);
  if (strMsec.size() < 4) {
    strMsec.prepend("0,");
  } else {
    strMsec.insert(strMsec.size() - 3, ',');
  }

  return strMsec;
}
