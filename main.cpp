#include <QApplication>
#include <QTranslator>

#include "mainwindow.h"
#include "techfiles/rtwtypes.h"

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  QTranslator t;

  // Строка включения английского перевода
  if (/* DISABLES CODE */ (false)) {
    t.load(":/english.qm");
    a.installTranslator(&t);
  }

  MainWindow mw;
  mw.show();

  return a.exec();
}
