<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB" sourcelanguage="ru_RU">
<context>
    <name>ChartWindow</name>
    <message>
        <location filename="chartwindow.ui" line="20"/>
        <source>График</source>
        <translation>Chart</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>ChaoMess</source>
        <translation>ChaoMess</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="60"/>
        <source>Передатчик</source>
        <translation>Transmitter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="152"/>
        <source>Канал связи</source>
        <translation>Com. channel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <source>График</source>
        <translation>Chart</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="222"/>
        <source>Отправить
сообщение</source>
        <translation>Send
message</translation>
    </message>
    <message>
        <source>Передать
сообщение</source>
        <translation type="vanished">Send
message</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="235"/>
        <source>Приёмник</source>
        <translation>Receiver</translation>
    </message>
</context>
</TS>
