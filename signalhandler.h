#ifndef SIGNALCREATOR_H
#define SIGNALCREATOR_H

#include <QString>
#include <QDebug>
#include <QByteArray>
#include <QVector>
#include <QTextCodec>
#include <iostream>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "techfiles/observermodel.h"

class SignalHandler
{
public:
    SignalHandler();
    QVector<QByteArray> StrToBit(QString Message);
    QString BitToStr(QVector<QByteArray> Message);
    QByteArray BitsToBytes(QBitArray bits);
    void SendSignal(QVector<QByteArray> message, int messageSize, QVector<real_T>& transmitterData,
                    QVector<real_T>& receiverData, QVector<time_T>& sendedSignal, QVector<time_T>& time);
    QVector<QByteArray> ReceiveSignal(int messageSize, QVector<real_T>& receiverData, QVector<QByteArray>& recievedByteMessage);
};

#endif // SIGNALCREATOR_H
