#ifndef RTW_HEADER_Observer_model_h_
#define RTW_HEADER_Observer_model_h_
#include <stddef.h>
#include <cmath>
#include <math.h>
#include <string.h>
#include <QVector>
#include <QDebug>
#ifndef Observer_model_COMMON_INCLUDES_
# define Observer_model_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 // Observer_model_COMMON_INCLUDES_

// Macros for accessing real-time model data structure
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

// Forward declaration for rtModel
typedef struct tag_RTM RT_MODEL;

// Block signals and states (auto storage) for system '<Root>'
typedef struct {
  real_T FromWs;                       // '<S5>/FromWs'
  real_T Integrator5;                  // '<S2>/Integrator 5'
  real_T Integrator1;                  // '<S3>/Integrator 1'
  real_T estimation;                   // '<S2>/estimation'
  real_T Sum;                          // '<S2>/Sum'
  real_T Sum_m;                        // '<Root>/Sum'
  real_T Integrator4;                  // '<S2>/Integrator 4'
  real_T dvdt;                         // '<S2>/dv//dt'
  real_T Integrator2;                  // '<S3>/Integrator 2'
  real_T Integrator3;                  // '<S3>/Integrator 3'
  real_T Sum_e;                        // '<S3>/Sum'
  real_T dzdt;                         // '<S3>/dz//dt'
  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWs_PWORK;                      // '<S5>/FromWs'

  struct {
    void *LoggedData;
  } errort_PWORK;                      // '<Root>/error(t)'

  struct {
    void *LoggedData;
  } mut_PWORK;                         // '<Root>/mu(t)'

  struct {
    void *LoggedData;
  } mu_estimatedt_PWORK;               // '<Root>/mu_estimated(t)'

  struct {
    void *LoggedData;
  } zt_PWORK;                          // '<Root>/z(t)'

  struct {
    void *LoggedData;
  } Xvt_PWORK;                         // '<S2>/Xv(t)'

  struct {
    void *LoggedData;
  } Yvt_PWORK;                         // '<S2>/Yv(t)'

  struct {
    void *LoggedData;
  } estt_PWORK;                        // '<S2>/est(t)'

  struct {
    void *LoggedData;
  } X1t_PWORK;                         // '<S3>/X1(t)'

  struct {
    void *LoggedData;
  } Yt_PWORK;                          // '<S3>/Y(t)'

  struct {
    void *LoggedData;
  } Z1t_PWORK;                         // '<S3>/Z1(t)'

  struct {
    void *LoggedData;
  } bt_PWORK;                          // '<S3>/b(t)'

  struct {
    int_T PrevIndex;
  } FromWs_IWORK;                      // '<S5>/FromWs'

  uint32_T RandSeed;                   // '<S1>/Uniform Random Number'
} DW;

// Continuous states (auto storage)
typedef struct {
  real_T Integrator6_CSTATE;           // '<S2>/Integrator 6'
  real_T Integrator5_CSTATE;           // '<S2>/Integrator 5'
  real_T Integrator1_CSTATE;           // '<S3>/Integrator 1'
  real_T Integrator4_CSTATE;           // '<S2>/Integrator 4'
  real_T Integrator2_CSTATE;           // '<S3>/Integrator 2'
  real_T Integrator3_CSTATE;           // '<S3>/Integrator 3'
} X;

// State derivatives (auto storage)
typedef struct {
  real_T Integrator6_CSTATE;           // '<S2>/Integrator 6'
  real_T Integrator5_CSTATE;           // '<S2>/Integrator 5'
  real_T Integrator1_CSTATE;           // '<S3>/Integrator 1'
  real_T Integrator4_CSTATE;           // '<S2>/Integrator 4'
  real_T Integrator2_CSTATE;           // '<S3>/Integrator 2'
  real_T Integrator3_CSTATE;           // '<S3>/Integrator 3'
} XDot;

// State disabled
typedef struct {
  boolean_T Integrator6_CSTATE;        // '<S2>/Integrator 6'
  boolean_T Integrator5_CSTATE;        // '<S2>/Integrator 5'
  boolean_T Integrator1_CSTATE;        // '<S3>/Integrator 1'
  boolean_T Integrator4_CSTATE;        // '<S2>/Integrator 4'
  boolean_T Integrator2_CSTATE;        // '<S3>/Integrator 2'
  boolean_T Integrator3_CSTATE;        // '<S3>/Integrator 3'
} XDis;

#ifndef ODE3_INTG
#define ODE3_INTG

// ODE3 Integration Data
typedef struct {
  real_T *y;                           // output
  real_T *f[3];                        // derivatives
} ODE3_IntgData;

#endif

// Real-time Model Data Structure
struct tag_RTM {
  const char_T *errorStatus;
  RTWSolverInfo solverInfo;
  X *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T blkStateChange;
  real_T odeY[6];
  real_T odeF[3][6];
  ODE3_IntgData intgData;

  //
  //  Sizes:
  //  The following substructure contains sizes information
  //  for many of the model attributes such as inputs, outputs,
  //  dwork, sample times, etc.

  struct {
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numSampTimes;
  } Sizes;

  //
  //  Timing:
  //  The following substructure contains information regarding
  //  the timing information for the model.

  struct {
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[3];
  } Timing;
};

#ifdef __cplusplus

extern "C" {

#endif

#ifdef __cplusplus

}
#endif

// Class declaration for model Observer_model
class ObserverModelClass {
  // public data and function members
 public:
  // model initialize function
  void initialize(double timeStepArg);

  // model step function
  void step0(QVector<real_T>& transmitterData, QVector<real_T>& receiverData,
  QVector<time_T>& sendedSignal, QVector<time_T>& time, qreal signal);

  // model step function
  void step2();

  // Constructor
  ObserverModelClass();

  // Destructor
  ~ObserverModelClass();

  // Real-Time Model get method
  RT_MODEL * getRTM();

  // private data and function members
 private:
  // Block signals and states
  DW rtDW;
  X rtX;                               // Block continuous states

  double timeStep = 0;
  int i = 0;

  // Real-Time Model
  RT_MODEL rtM;

  // Continuous states update member function
  void rt_ertODEUpdateContinuousStates(RTWSolverInfo* si, QVector<real_T>& transmitterData, QVector<real_T>& receiverData,
                                       QVector<time_T>& sendedSignal, QVector<time_T>& time, qreal signal);

  // Derivatives member function
  void Observer_model_derivatives();
};

//-
//  The generated code includes comments that allow you to trace directly
//  back to the appropriate location in the model.  The basic format
//  is <system>/block_name, where system is the system number (uniquely
//  assigned by Simulink) and block_name is the name of the block.
//
//  Use the MATLAB hilite_system command to trace the generated code back
//  to the model.  For example,
//
//  hilite_system('<S3>')    - opens system 3
//  hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
//
//  Here is the system hierarchy for this model
//
//  '<Root>' : 'Observer_model'
//  '<S1>'   : 'Observer_model/Information signal'
//  '<S2>'   : 'Observer_model/Receiver'
//  '<S3>'   : 'Observer_model/Transmitter'
//  '<S4>'   : 'Observer_model/Information signal/Chirp Signal'
//  '<S5>'   : 'Observer_model/Information signal/Signal Builder1'

#endif                                 // RTW_HEADER_Observer_model_h_
