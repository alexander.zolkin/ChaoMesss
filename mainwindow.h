#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCoreApplication>
#include <QApplication>
#include <QTime>
#include <QTranslator>
#include <stddef.h>
#include <stdio.h>

#include "chartwindow.h"
#include "techfiles/observermodel.h"
#include "techfiles/rtwtypes.h"
#include "ui_mainwindow.h"
#include "signalhandler.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbTransmitMessage_clicked();
    void on_pbShowChart_clicked();

private:
    bool disableDetiledInfo = false;

    QString _stringTransmitterDataText;
    QVector<double> _transmitterData;

    QString msecToSec(int msec);

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
