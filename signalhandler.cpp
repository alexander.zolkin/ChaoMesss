#include "signalhandler.h"

static ObserverModelClass rtObj;    // Instance of model class

// Associating rt_OneStep with a real-time clock or interrupt service routine
// is what makes the generated code "real-time".  The function rt_OneStep is
// always associated with the base rate of the model.  Subrates are managed
// by the base rate from inside the generated code.  Enabling/disabling
// interrupts and floating point context switches are target specific.  This
// example code indicates where these should take place relative to executing
// the generated code step function.  Overrun behavior should be tailored to
// your application needs.  This example simply sets an error status in the
// real-time model and returns from rt_OneStep.

void rtOneStep(QVector<real_T>& transmitterData, QVector<real_T>& receiverData,
               QVector<time_T>& sendedSignal, QVector<time_T>& time, qreal signal);
void rtOneStep(QVector<real_T>& transmitterData, QVector<real_T>& receiverData,
               QVector<time_T>& sendedSignal, QVector<time_T>& time, qreal signal)
{
  static boolean_T OverrunFlags[3] = { 0, 0, 0 };

  static boolean_T eventFlags[3] = { 0, 0, 0 };// Model has 3 rates

  static int_T taskCounter[3] = { 0, 0, 0 };

  // Disable interrupts here

  // Check base rate for overrun
  if (OverrunFlags[0]) {
    rtmSetErrorStatus(rtObj.getRTM(), "Overrun");
    return;
  }

  OverrunFlags[0] = true;

  // Save FPU context here (if necessary)
  // Re-enable timer or interrupt here

  //
  //  For a bare-board target (i.e., no operating system), the
  //  following code checks whether any subrate overruns,
  //  and also sets the rates that need to run this time step.

  if (taskCounter[2] == 0) {
    if (eventFlags[2]) {
      OverrunFlags[0] = false;
      OverrunFlags[2] = true;

      // Sampling too fast
      rtmSetErrorStatus(rtObj.getRTM(), "Overrun");
      return;
    }

    eventFlags[2] = true;
  }

  taskCounter[1]++;
  if (taskCounter[1] == 1) {
    taskCounter[1]= 0;
  }

  taskCounter[2]++;
  if (taskCounter[2] == 1000) {
    taskCounter[2]= 0;
  }

  // Set model inputs associated with base rate here

  // Step the model for base rate
  rtObj.step0(transmitterData, receiverData, sendedSignal, time, signal);

  // Get model outputs here

  // Indicate task for base rate complete
  OverrunFlags[0] = false;

  // If task 1 is running, don't run any lower priority task
  if (OverrunFlags[1]) {
    return;
  }

  // Step the model for subrate
  if (eventFlags[2]) {
    OverrunFlags[2] = true;

    // Set model inputs associated with subrates here

    // Step the model for subrate 2
    rtObj.step2();

    // Get model outputs here

    // Indicate task complete for subrate
    OverrunFlags[2] = false;
    eventFlags[2] = false;
  }

  // Disable interrupts here
  // Restore FPU context here (if necessary)
  // Enable interrupts here
}

SignalHandler::SignalHandler(){ }

QVector<QByteArray> SignalHandler::StrToBit(QString message)
{
  QVector<QByteArray> byteArMessage(message.count());

  for (int i = 0; i < message.size(); ++i){
    byteArMessage[i].setNum(message[i].unicode(), 2);
  }

  return byteArMessage;
}

QString SignalHandler::BitToStr(QVector<QByteArray> message)
{
  QString reconstructedMessage;
  char16_t utf16codeunit;
  for (int i = 0; i < message.size(); ++i){
    utf16codeunit = static_cast<char16_t>(message[i].toInt(nullptr, 2));
    char16_t str[] = {utf16codeunit, 0};
    reconstructedMessage.append(QString::fromUtf16(str));
  }

  return reconstructedMessage;
}

void SignalHandler::SendSignal(QVector<QByteArray> message, int messageSize, QVector<real_T>& transmitterData,
                               QVector<real_T>& receiverData, QVector<time_T>& sendedSignal, QVector<time_T>& time)
{
  qreal signal = 14;

  // Initialize model
  double timeStep = 0.0001;
  rtObj.initialize(timeStep);

  for (int i = 0; i < 10000; ++i){
      rtOneStep(transmitterData, receiverData, sendedSignal, time, signal);
  }

    for (int j = 0; j < message.size(); ++j) {
    for (int k = 0; k < message[j].size(); ++k) {
      if (message[j][k] == '1'){
        signal = 18;
      } else if (message[j][k] == '0') {
        signal = 14;
      }

      for (int e = 0; e < messageSize; ++e){
        rtOneStep(transmitterData, receiverData, sendedSignal, time, signal);
      }

      if (message[j].size() - k == 1){
        signal = 22;
        for (int e = 0; e < messageSize; ++e){
            rtOneStep(transmitterData, receiverData, sendedSignal, time, signal);
        }
      }
    }
  }
}

QVector<QByteArray> SignalHandler::ReceiveSignal(int messageSize, QVector<real_T>& receiverData, QVector<QByteArray>& recievedByteMessage)
{
  QByteArray recievedByteSymbol;
  qreal mean = 0;
  int indentation = messageSize - 100;
  receiverData.erase(receiverData.begin(), receiverData.begin()+10000);

  QVector<real_T> tmp;
  for (int i = 0; i < receiverData.size() / messageSize; ++i){
    tmp = receiverData;
    tmp.erase(tmp.begin() + (i * messageSize) + messageSize, tmp.end());
    tmp.erase(tmp.begin(), tmp.begin() + (i * messageSize) + indentation);

    for (int j = 0; j < tmp.size(); ++j){
        mean += tmp[j];
    }

    mean /= tmp.size();

    if (mean > 12 && mean <= 16){
        recievedByteSymbol.append('0');
    } else if (mean > 16 && mean <= 20){
        recievedByteSymbol.append('1');
    } else if (mean > 20 && mean <= 24){
        recievedByteMessage.append(recievedByteSymbol);
        recievedByteSymbol.clear();
    } else {
        qDebug() << "Ошибка сигнала";
    }

    mean = 0;
  }

  return recievedByteMessage;
}
